extends Node2D

# Called every frame. 'delta' is the elapsed time since the previous frame.
var enemyTmp = preload("res://enemy.tscn")
var enemylist = []
var target = 0
var numRounds = 3
@onready var player
@onready var camera
@onready var victoryScreen

@onready var Wraith
@onready var Slime
@onready var Slime2
@onready var Skeleton
@onready var Boss

func _init():
	#enemy1 = enemy.instantiate()
	#enemy1.position = (Vector2(0,0))
	#add_child(enemy1)
	#enemy2 = enemy.instantiate()
	#enemy2.position = (Vector2(-200, -10))
	#add_child(enemy2)
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	target = 0
	Wraith = get_node("res://Wraith.gd")
	Slime = get_node("res://Slime.gd")
	Slime2 = get_node("res://Slime2.gd")
	Skeleton = get_node("res://Skeleton.gd")
	Boss = get_node("res://Boss.gd")
	player = get_node("/root/Node2D/GUI/Player")
	camera = get_node("/root/Node2D/Camera2D")
	victoryScreen = get_node("/root/Node2D/VictoryScreen")
	#spawn_enemy("slime")
	#spawn_enemy("slime2")
	#spawn_enemy("wraith")
	#spawn_enemy("skeleton")
	spawn_enemy("slime2")
	spawn_enemy("slime")
	spawn_enemy("slime")
	pass # Replace with function body.


func spawn_enemy(type):
	var n = enemylist.size()
	enemylist.append(enemyTmp.instantiate())
	match type:
		"slime":
			type = "res://Slime.gd"
		"slime2":
			type = "res://Slime2.gd"
		"wraith":
			type = "res://Wraith.gd"
		"skeleton":
			type = "res://Skeleton.gd"
		"boss":
			type = "res://boss.gd"
		_:
			push_error("Enemy ID not create")
	enemylist[-1].set_script(load(type))
	print(enemylist[-1])
	enemylist[-1].position = (Vector2(n*-200,20*(-1**n)))
	enemylist[-1].attack.connect(player._on_enemy_attack)
	enemylist[-1].targeted.connect(self._on_enemy_targeted)
	enemylist[-1].died.connect(self._on_enemy_died)
	enemylist[-1].posNum = n
	add_child(enemylist[-1])
	changeTarget(n)
	
func changeTarget(num, deselect = true):
	if deselect:
		enemylist[target].deselect()
	enemylist[num].select()
	target = num
	pass

func dealDamage(amount, piercing=false, type=null, all = false, rand = 0):
	print("damage", amount)
	if all:
		for en in enemylist:
			en.takeDamage(amount, piercing, type)
	else:
		if rand == 0 && enemylist:
			enemylist[target].takeDamage(amount, piercing, type)
		else:
			for n in range(rand):
				changeTarget(randi_range(0, enemylist.size() - 1))
				enemylist[target].takeDamage(amount, piercing, type)
	pass

func addWeakness():
	enemylist[target].weak = 1

func afflictStatus(statusName, amount, numTurns):
	if(statusName == "Fire"):
		enemylist[target].fireDamage += amount
		if(enemylist[target].fireDuration < numTurns):
			enemylist[target].fireDuration = numTurns
	else:
		if(statusName == "Poison"):
			enemylist[target].poisonDamage += amount
			if(enemylist[target].poisonDuration < numTurns):
				enemylist[target].poisonDuration = numTurns
		else:
			if(statusName == "Bleed"):
				enemylist[target].bleedDamage += amount
				if(enemylist[target].bleedDuration < numTurns):
					enemylist[target].bleedDuration = numTurns
			else:
				if(statusName == "Stun"):
					enemylist[target].stunnedDuration += amount
	enemylist[target].updateStatus()

func _on_enemy_targeted(num):
	changeTarget(num)

func _on_enemy_died(num):
	print("enemy ", num, " died")
	#print("123")
	var temp = enemylist[num]
	temp.heal(1000) # restore health for next spawn
	enemylist.remove_at(num)
	if enemylist.size() == 0:
		winFight()
	else:
		for x in enemylist.size():
			enemylist[x].posNum = x
		if target == num:
			changeTarget(enemylist.size()-1, false)
		temp.queue_free()	#deletes enemy node
		
func winFight():
	print("Fight won")
	
	camera.position = Vector2(575, 972) # move to victory screen
	victoryScreen.initScreen()
	restore()
	if(numRounds > 0):
		numRounds -= 1
		var fightlist = randomFight()
		#spawn_enemy("slime2")
		#spawn_enemy("slime")
		#spawn_enemy("slime")
		for x in fightlist:
			spawn_enemy(x)
			#print(x)
		#pass
		for x in enemylist.size():
			pass
			#print(x)
			#enemylist[x].heal(200)
	else:
		numRounds = 3
		spawn_enemy("boss")
	
func restore():
	pass
	#Wraith._ready()
	#Slime._ready()
	#Slime2._ready()
	#Skeleton._ready()
	#Boss._ready()

func randomFight():
	var fightList = [
		#["slime", "slime2","slime"],
		["slime2", "slime","slime2"],
		["skeleton","skeleton"],
		["wraith","slime"],
		["slime", "skeleton"],
		["wraith", "skeleton", "slime"],
	]
	return fightList[randi() % fightList.size()]
