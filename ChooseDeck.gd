extends Node2D

var deck
var theDeck
# Called when the node enters the scene tree for the first time.
func _ready():
	deck = get_node("/root/LOADER")
	#deck = get_node("res://Node2D/Deck")
	#var deck_scene = preload("res://node_2d.tscn") 
	#deck = deck_scene.get_node("Deck")
	theDeck = deck.deck

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_warrior_button_pressed():
	theDeck.append_array([7, 6, 5, 4, 4, 3, 3, 2, 2, 1, 1, 1, 1, 1, 1])
	get_tree().change_scene_to_file("res://node_2d.tscn")


func _on_ranger_button_pressed():
	theDeck.append_array([18, 17, 16, 15, 14, 13, 13, 12, 12, 11, 11, 11, 11, 11, 11])
	get_tree().change_scene_to_file("res://node_2d.tscn")


func _on_wizard_button_pressed():
	theDeck.append_array([28, 27, 26, 25, 24, 24, 23, 23, 22, 22, 21, 21, 21, 21, 21, 21, 21, 21])
	get_tree().change_scene_to_file("res://node_2d.tscn")


func _on_mixed_button_pressed():
	# add basic cards, then random cards
	theDeck.append_array([22, 21, 21, 12, 11, 11, 2, 1, 1])
	var rand = (randi() % 5) + 6 # rand between 6 and 10
	for i in rand:
		theDeck.append_array([(randi() % 30) + 1]) # rand between 1 and 30, inserts that card
	get_tree().change_scene_to_file("res://node_2d.tscn")
