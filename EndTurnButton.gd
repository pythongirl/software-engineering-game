extends Node2D


func _ready():
	var button = Button.new()
	button.text = "End\nTurn"
	button.pressed.connect(self._button_pressed)
	add_child(button)

func _button_pressed():
	#end the players turn
