1)
Fixing errors with displaying cards from your hand in the game view. The array was not showing up properly in hand.gd, 
so I figured out the problem using print statements to find the location. I was adding the variables into the array, 
which is not proper syntax, so I changed these to append.

I also forgot to change ints to strings when finding the proper pathnames, so I adjusted that as well.

2) 
The size is not editable, forcing the cards to be a large size. *Fix: Instead of using a horizontal gui, which
forces a certain layout, use a free-form gui to allow different sizes. *

3)
I had a problem with the relative paths to nodes. After doing some research, it appears that relative paths don't work 
as intended between different scenes. To fix this, I implemented the full path in my code.

4)
When allowing the card to move, it can move outside the bounds of the window, locking it out of frame. To fix this, I
changed the window to force the mouse inside it when holding a card.

5) 
When moving a card, it would not activate, and draging other cards would freeze and move nearby cards. Fixed this by 
editing collision boxes, and correcting the horizontal line activation function.

6) ***
Deck has incorrect order of cards: 
Not fixed yet. Right now, the deck displays the cards right to left, which makes it a bit strange when you are dealt a
new hand. This is not a big issue, so this is low on our priority list.

7)
The deck does not shuffle at the beginning of the encounter. This functionality will need to be added by the time we
have encounters set up. (Done)

8)
The enemies block disappeared, and broke the game after two turns. To fix this, we added a block that doesn't delete 
itself, and modified the stick figure enemy to be a slime enemy.

9)
Piercing damage from ranger cards counted as normal damage. To fix this, we added an extra check to
the enemy takeDamage function that checks if the damage is not peircing before it tries to block.

10)
Cards that added weakness did nothing. Weakness cards now add a permanent 1.5 damage taken multiplier to enemies.

11)
Error when trying to deal damage after enemy dies but without clicking a new target. Enemy death now automatically
changes target to front of 

)
Cards and end turn button not functional after adding background. Fixed by moving background to top of tree rather 
than using z axis to draw in back
