extends Node2D

var hand = []
#var indexOn; # this variable is the card you are on in the hand
# Called when the node enters the scene tree for the first time.
func _ready():
	# test; 4 cards with id 1 in the hand
	var theDeck = $"../Deck"
	theDeck.drawHand();
	displayHand()
	#pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func displayHand():
	#Note: The below will be edited later to have the proper description and card name
	var theDeck = $"../Deck"

	var handSize = hand.size()
	for i in handSize:
		GUI.cardOn = i+1
		# grab variables from deck, pass cardID to function first
		theDeck.cardID = hand[i]
		theDeck.getCardInfo()
		
		GUI.desc = theDeck.desc
		GUI.cardName = theDeck.cardName
		GUI.energy = theDeck.energy
		GUI.picture = theDeck.picture
		GUI.cardPic = theDeck.cardPic
		GUI.setCard()
	for j in (5-handSize):
		var nodeName = "/root/Node2D/GUI/HandDisplay/KinematicBody" + str(j + handSize + 1) + "/Card" + str((j + handSize + 1)) # gets Card1, Card2, ect.
		var myCard = get_node(nodeName)
		myCard.visible = false
	
	
#var useID = 0
#func useCard():
#	print("success", useID)
	
	
