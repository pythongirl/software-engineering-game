
Must-
- Use Godot as the Game Engine
- Be able to acquire new cards for the deck
- Be able to remove cards from the deck
- Cards have a energy cost
- Player must have an energy system
- Player should have a health bar
- Enemy drops 
- Card pickups must be optional
- Player has turn, then enemy has turn
- Have a boss 
- Game has a main menu
- Have unique artwork for each enemy
- When deck is depleted the discard pile is reshuffled and drawn from
- Player gets 2 energy per turn

Should-
- Be able to upgrade cards
- Some enemies can have armor
- Have 10 cards per archetype.
- Have status effects
- Add a shop
- Enemies have an attack pattern 
- Have 3 backgrounds for rooms 
- Add a miniboss 
- Miniboss drops upgraded cards
- Have a hand size of 6
- Have exile cards that can be used once
- Have random event rooms
- Allow player to save energy (up to a max of 3)
- Should have cards that give buffs
- Allow player to choose their starting deck type
- Should load the screen in less than 10 seconds
- Should not be able to access network for security purposes
- Card designs finished in 2 weeks
- Have some option to heal character health, but it should come at the cost of something else
- Have sound effects (recorded or from online)
- Have different difficulty settings
- Game should last between 15-30 minutes of play time.

Could-
- Add an item system
- Could have Player moving to different areas
- Have a boss background
- Could have music 
- Make a score system
- Endless mode 
- Have a system of sacrificing max health and gaining immensely powerful cards
- Add a save system
- Add easter eggs 

Won’t-
- Will not have unlockable cards
- Player will not have a skill tree
- Multiple characters 
- Have different scales of health for players and enemies
- Won’t have a max deck size



User Stories:
1. The player wants to get a new card for their deck. They go into an encounter with enemies, and defeats them quickly. Afterward, the player is given the choice of one of three new cards, or no cards.
2. The player is low on health, and wants to heal up. They find a place that will heal them, or give them a card upgrade. They choose to heal their health, and receive +50% health. They continue onwards, now with full health.
3. The player wishes to remove a weak card from their deck. They go to a shop and pay to remove 1 card. They then leave the shop. 
4. The player wants to change the sound effect volume in the game. They go to the setting menu from the main menu or the pause menu. Then the player will drag the sound effects volume slider and then return to their game.
5. The player gets to the final boss, and triumphantly defeats the boss. Once they do this, a “You won!” screen will appear. After this, it takes them back to the main menu.
