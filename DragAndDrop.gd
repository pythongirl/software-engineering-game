extends RigidBody2D

var dragging = false
var originalPos = self.position
signal dragsignal;
signal nondragsignal;

@onready var kine1

func _ready():
	connect("dragsignal",self._set_drag_pc)
	connect("nondragsignal",self._set_nondrag_pc)
	kine1 = get_node("/root/Node2D/GUI/HandDisplay/KinematicBody1")
	
	
func _process(_delta):
	if dragging:
		#print(kine1.position)
		#print((ProjectSettings.get_setting("display/window/size/viewport_height") / 2))
		var mousepos = get_viewport().get_mouse_position()
		var cardWidth = 240
		var cardHeight = 288
		self.position = Vector2(mousepos.x - (cardWidth/2) + 35, mousepos.y - (cardHeight/2) + 25)
	else:
		self.position = originalPos
		

func _set_drag_pc():
	dragging= true
	
func _set_nondrag_pc():
	dragging= false


func _on_KinematicBody2D_input_event(_viewport, event, _shapeidx):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed and dragging == false:
			# dragging
			self.position = originalPos
			emit_signal("dragsignal")
			
			# mouse cannot leave window while dragging
			Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		elif event.button_index == MOUSE_BUTTON_LEFT and !event.pressed and dragging == true:
			# dropping
			emit_signal("nondragsignal")
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			if((self.position.y + 144) < (ProjectSettings.get_setting("display/window/size/viewport_height") / 2)):
				self.position = originalPos
				# activate card if enough energy
				#print("yeppers")
				var cardIndex = int(self.name.substr(self.name.length() - 1, 1)) - 1 # returns 0 through 4
				var myDeck = get_node("/root/Node2D/Deck")
				myDeck.useID = cardIndex
				myDeck.activateCard()
			else:
				# return card to original postion
				self.position = originalPos



