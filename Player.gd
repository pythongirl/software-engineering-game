extends Node2D

var maxHealth
var currentHealth
var magicOrbs = 0
var dodge = 0
var block = 0
var resist = 'none'
var weak = 0
var energyCurrent
var energyMax = 3
@onready var energyText
@onready var healthBar
@onready var healthBarText
@onready var blockNode
@onready var blockText
@onready var dodgeNode
@onready var dodgeText
@onready var orbNode
@onready var orbText
@onready var dieNode

# Called when the node enters the scene tree for the first time.
func _ready():
	energyCurrent = energyMax
	maxHealth = 80
	currentHealth = maxHealth	
	healthBar = get_node("/root/Node2D/GUI/Player/HealthBar")
	healthBarText = get_node("/root/Node2D/GUI/Player/HealthBar/HealthText")
	blockNode = get_node("/root/Node2D/GUI/Player/Block")
	blockText = get_node("/root/Node2D/GUI/Player/Block/BlockText")
	dodgeNode = get_node("/root/Node2D/GUI/Player/Dodge")
	dodgeText = get_node("/root/Node2D/GUI/Player/Dodge/DodgeText")
	orbNode = get_node("/root/Node2D/GUI/Player/Orb")
	orbText = get_node("/root/Node2D/GUI/Player/Orb/OrbText")
	energyText = get_node("/root/Node2D/GUI/Energy/EnergyText")
	dieNode = get_node("/root/Node2D/GUI/DieText")
	healthBar.max_value = maxHealth
	updateEnergy()
	updateHealth()

func updateHealth():
	healthBar.value = currentHealth
	healthBarText.text = str(currentHealth) + "/" + str(maxHealth)
	
func updateEnergy():
	energyText.text = str(energyCurrent) + "/" + str(energyMax)
	
func addEnergy(amount):
	energyCurrent += amount
	if (energyCurrent > energyMax):
		energyCurrent = energyMax
	updateEnergy()

func addOrb(amount):
	magicOrbs += amount
	if(magicOrbs > 0):
		orbNode.visible = true
	else:
		orbNode.visible = false
	orbText.text = str(magicOrbs)

func takeDamage(amount, type=null):
	if (dodge <= 0):
		if type == resist:
			amount = int((amount + 1) / 2)
		amount = int(amount * 1.5 ** weak)
		if block > 0:
			if block <= amount:
				amount -= block
				block = 0
				blockNode.visible = false
				blockText.text = str(block)
			else:
				block -= amount
				amount = 0
				blockText.text = str(block)
		if amount > 0:
			currentHealth -= amount
		if currentHealth <= 0:
			die()
		updateHealth()
	else: # if dodge is greater than 0
		addDodge(-1)
	#healthBar.value = currentHealth
		
func die():
	#game over 
	currentHealth = 0
	updateHealth()
	dieNode.visible = true
	pass

func heal(amount):
	currentHealth += amount
	if(currentHealth > maxHealth):
		currentHealth = maxHealth
	updateHealth()

func _on_enemy_attack(amount, type = null):
	#print("player recieve")
	takeDamage(amount, type)
	

func addDodge(amount):
	dodge += amount
	if(dodge > 0):
		dodgeNode.visible = true
	else:
		dodgeNode.visible = false
	dodgeText.text = str(dodge)

func addBlock(amount):
	block += amount
	if(block > 0):
		blockNode.visible = true
	else:
		blockNode.visible = false
	blockText.text = str(block)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
