extends TextureRect

var getCard = []
var rand = 0
@onready var camera
@onready var theDeck
@onready var explain
@onready var player
@onready var healButton
# Called when the node enters the scene tree for the first time.
func _ready():
	camera = get_node("/root/Node2D/Camera2D")
	theDeck = get_node("/root/Node2D/Deck")
	explain = get_node("/root/Node2D/VictoryScreen/Explain")
	player = get_node("/root/Node2D/GUI/Player")
	healButton = get_node("/root/Node2D/VictoryScreen/HealButtton")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func initScreen():
	healButton.visible = true
	player.energyCurrent = 3
	player.updateEnergy()
	player.addOrb(-player.magicOrbs)
	player.addDodge(-player.dodge)
	player.addBlock(-player.block)
	theDeck.resetDeck()
	
	displayCards()


func _on_next_room_pressed():
	theDeck.deck.shuffle()
	theDeck.drawHand()
	camera.position = Vector2(575, 324)


func displayCards():
	explain.text = ""
	getCard.clear()
	for i in range(3):
		rand = (randi() % 30) + 1
		getCard.append(rand)
		cardOn = i+1
		# grab variables from deck, pass cardID to function first
		theDeck.cardID = rand
		theDeck.getCardInfo()
		
		desc = theDeck.desc
		cardName = theDeck.cardName
		energy = theDeck.energy
		picture = theDeck.picture
		cardPic = theDeck.cardPic
		setCard()


#parameters to change
var cardOn #index of card to change
var desc
var cardName
var energy
var picture
var cardPic

# functions to call
func setCard():
	var nodeName = "/root/Node2D/VictoryScreen/Card" + str(cardOn)# gets Card1, Card2, ect.
	var myCard = get_node(nodeName)
	
	myCard.visible = true
	myCard.get_node("Name").text = cardName
	myCard.get_node("Desc").text = desc
	myCard.get_node("EnergyCost/EnergyNumber").text = str(energy)
	myCard.get_node("Picture").texture = load(picture)
	myCard.texture = load(cardPic)
	# change picture as well


func _on_card_1_button_pressed():
	theDeck.deck.append(getCard[0])
	explain.text = "Card Added: " + get_node("/root/Node2D/VictoryScreen/Card1/Name").text
	for i in range(3):
		var nodeName = "/root/Node2D/VictoryScreen/Card" + str(i+1)# gets Card1, Card2, ect.
		var myCard = get_node(nodeName)
		myCard.visible = false
		


func _on_card_2_button_pressed():
	theDeck.deck.append(getCard[1])
	explain.text = "Card Added: " + get_node("/root/Node2D/VictoryScreen/Card2/Name").text
	for i in range(3):
		var nodeName = "/root/Node2D/VictoryScreen/Card" + str(i+1)# gets Card1, Card2, ect.
		var myCard = get_node(nodeName)
		myCard.visible = false


func _on_card_3_button_pressed():
	theDeck.deck.append(getCard[2])
	explain.text = "Card Added: " + get_node("/root/Node2D/VictoryScreen/Card3/Name").text
	for i in range(3):
		var nodeName = "/root/Node2D/VictoryScreen/Card" + str(i+1)# gets Card1, Card2, ect.
		var myCard = get_node(nodeName)
		myCard.visible = false


func _on_heal_buttton_pressed():
	healButton.visible = false
	player.heal(20)
	var index = randi() % theDeck.deck.size()
	var cardID = theDeck.deck[index]
	theDeck.cardID = cardID
	theDeck.getCardInfo()
	explain.text = "+20 HP, Card Removed: " + theDeck.cardName
	
	theDeck.deck.remove_at(index)
	
	pass # Replace with function body.
