extends Node2D

var maxHealth
var currentHealth
var block = 0
var resist = 'none'
var weak = 0
var selfDamage = 0
var stunnedDuration = 0 
var damageAmount = 0
var fireDamage = 0
var fireDuration = 0
var poisonDamage = 0
var poisonDuration = 0
var bleedDamage = 0
var bleedDuration = 0
var dodge = 0
@onready var healthBar
@onready var healthBarText
@onready var blockNode
@onready var blockText
@onready var fire
@onready var firedamagetext
@onready var firedurationtext
@onready var poison
@onready var poisondamagetext
@onready var poisondurationtext
@onready var bleed
@onready var bleeddamagetext
@onready var bleeddurationtext
@onready var intentNode
@onready var intentText
@onready var dodgeNode
@onready var dodgeText

var intent
@onready var player
@onready var selectTexture
var posNum
signal targeted
signal attack
signal died

# Called when the node enters the scene tree for the first time.
func _ready():
	setBase()
	setHealth(50,100)
	setIntent()
	getAllNodes()
	pass

func setBase():
	#on initialize of each enemy
	selfDamage = 0
	block = 0
	resist = 'none'
	weak = 0
	stunnedDuration = 0
	damageAmount = 0
	fireDamage = 0
	fireDuration = 0
	poisonDamage = 0
	poisonDuration = 0
	bleedDamage = 0
	bleedDuration = 0

func setHealth(low, high):
	maxHealth = randi_range(low,high)
	currentHealth = maxHealth

func setIntent():
	intent = 0
	#intent = randi_range(0,2)

func getAllNodes():
	healthBar = get_node("HealthBar")
	healthBarText = get_node("HealthBar/HealthText")
	selectTexture = get_node("Select")
	blockNode = get_node("BlockNode")
	blockText = get_node("BlockNode/BlockText")
	fire = get_node("Fire")
	firedamagetext = get_node("Fire/FireDamageText")
	firedurationtext = get_node("Fire/FireDurationText")
	poison = get_node("Poison")
	poisondamagetext = get_node("Poison/PoisonDamageText")
	poisondurationtext = get_node("Poison/PoisonDurationText")
	bleed = get_node("Bleed")
	bleeddamagetext = get_node("Bleed/BleedDamageText")
	bleeddurationtext = get_node("Bleed/BleedDurationText")
	intentNode = get_node("IntentNode")
	intentText = get_node("IntentNode/IntentText")
	dodgeNode = get_node("Dodge")
	dodgeText = get_node("Dodge/DodgeText")
	get_node("/root/Node2D/GUI/EndTurnButton").pressed.connect(_on_end_turn_button_pressed)
	player = get_node("/root/Node2D/GUI/Player")
	healthBar.max_value = maxHealth
	updateHealth()


func updateHealth():
	healthBar.value = currentHealth
	healthBarText.text = str(currentHealth) + "/" + str(maxHealth)
	
func continousDamage():
	# Note: Continous damage should not take weakness or resistance into account
	damageAmount = 0
	if(bleedDuration > 0):
		damageAmount += bleedDamage
		bleedDuration -= 1
	if(poisonDuration > 0):
		damageAmount += poisonDamage
		poisonDuration -= 1
	if(fireDuration > 0):
		damageAmount += fireDamage
		fireDuration -= 1
	
	if block > 0:
		if block <= damageAmount:
			damageAmount -= block
			block = 0
			blockNode.visible = false
			blockText.text = str(block)
		else:
			block -= damageAmount
			damageAmount = 0
			blockText.text = str(block)
	if damageAmount > 0:
		currentHealth -= damageAmount
	if currentHealth < 0:
		print("huh", currentHealth)
		die()
	updateHealth()
	updateStatus()
	

func takeDamage(amount, piercing=false, type=null):
	if(dodge == 0):
		if type == resist:
			amount = int((amount + 1) / 2)
		amount = int(amount * 1.5 ** weak)
		if not(piercing) && block > 0:
			if block <= amount:
				amount -= block
				block = 0
				blockNode.visible = false
				blockText.text = str(block)
			else:
				block -= amount
				amount = 0
				blockText.text = str(block)
		if amount > 0:
			currentHealth -= amount
		if currentHealth <= 0:
			die()
		updateHealth()
	else: # if dodge is greater than 0
		addDodge(-1)

func heal(amount):
	currentHealth += amount
	if(currentHealth > maxHealth):
		currentHealth = maxHealth

func addDodge(amount):
	dodge += amount
	if(dodge > 0):
		dodgeNode.visible = true
	else:
		dodgeNode.visible = false
	dodgeText.text = str(dodge)

func die():
	self.visible = false
	#print("error2")
	died.emit(posNum)
	#queue_free()
	pass

func doTurn():
	if(selfDamage > 0):
		takeDamage(selfDamage)
	if(stunnedDuration > 0):
		stunnedDuration -= 1
	else:
		# doTurn stuff here
		pass

func _on_end_turn_button_pressed():
	#print("enemy recieved")
	player.addEnergy(2)
	doTurn()
	continousDamage()
	endTurn()
	
func endTurn():
	# Note: this function is not completed, all it does right now is restore player hand
	var deck = get_node("/root/Node2D/Deck")
	deck.drawHand()
	
	
func select():
	selectTexture.visible = true
	#selectTexture.texture = load("res://Sprites/Box (1).png")
	#selectTexture.show()
	pass
func deselect():
	selectTexture.visible = false
	#selectTexture.hide()
	pass

func addBlock(amount):
	block += amount
	blockNode.visible = true
	blockText.text = str(block)
	
func _input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			print("clicked")
			targeted.emit(posNum)
			
func updateStatus():
	if(fireDuration > 0):
		fire.visible = true
		firedamagetext.text = str(fireDamage)
		firedurationtext.text = str(fireDuration)
	else:
		fire.visible = false
	if(poisonDuration > 0):
		poison.visible = true
		poisondamagetext.text = str(poisonDamage)
		poisondurationtext.text = str(poisonDuration)
	else:
		poison.visible = false
	if(bleedDuration > 0):
		bleed.visible = true
		bleeddamagetext.text = str(bleedDamage)
		bleeddurationtext.text = str(bleedDuration)
	else:
		bleed.visible = false
	
	pass
