extends "res://Enemy.gd"


# Called when the node enters the scene tree for the first time.
func _ready():
	#maxHealth = randi_range(18,22)
	#currentHealth = maxHealth
	#intent = randi_range(0,2)
	#healthBar = get_node("HealthBar")
	#healthBarText = get_node("HealthBar/HealthText")
	#selectTexture = get_node("Select")
	#blockNode = get_node("Block")
	#blockText = get_node("Block/BlockText")
	#healthBar.max_value = maxHealth
	#updateHealth()
	#get_node("/root/Node2D/GUI/EndTurnButton").pressed.connect(_on_end_turn_button_pressed)
	setHealth(10,10)
	setIntent()
	getAllNodes()
	get_node("TextureRect").texture = load("res://Sprites/Wraith.png")

	
#	pass # Replace with function body.

func setIntent():
	intent = randi_range(0,2)

#func getAllNodes():
#	healthBar = get_node("HealthBar")
#	healthBarText = get_node("HealthBar/HealthText")
#	selectTexture = get_node("Select")
#	blockNode = get_node("Block")
#	blockText = get_node("Block/BlockText")
#	get_node("/root/Node2D/GUI/EndTurnButton").pressed.connect(_on_end_turn_button_pressed)
#	healthBar.max_value = maxHealth
#	updateHealth()
	
	
func doTurn():
	match intent:
		0:
			attack.emit(5)
			intentNode.texture = load("res://Sprites/new dodge.png")
			intentText.text = "1"
		1:
			#addBlock(5)
			addDodge(1)
			intentNode.texture = load("res://Sprites/attack.png")
			intentText.text = "7"
			pass
		2:
			attack.emit(7)
			intentNode.texture = load("res://Sprites/attack.png")
			intentText.text = "5"
			pass
	intent +=1
	if intent > 2:
		intent = 0
	pass
	
func takeDamage(amount, piercing=false, type=null):
	amount = 1
	if(dodge == 0):
		if type == resist:
			amount = int((amount + 1) / 2)
		amount = int(amount * 1.5 ** weak)
		if not(piercing) && block > 0:
			if block <= amount:
				amount -= block
				block = 0
				blockNode.visible = false
				blockText.text = str(block)
			else:
				block -= amount
				amount = 0
				blockText.text = str(block)
		if amount > 0:
			currentHealth -= amount
		if currentHealth <= 0:
			die()
		updateHealth()
	else: # if dodge is greater than 0
		addDodge(-1)	

func continousDamage():
	# Note: Continous damage should not take weakness or resistance into account
	damageAmount = 0
	if(bleedDuration > 0):
		damageAmount += 1
		bleedDuration -= 1
	if(poisonDuration > 0):
		damageAmount += 1
		poisonDuration -= 1
	if(fireDuration > 0):
		damageAmount += 1
		fireDuration -= 1
		
	if block > 0:
		if block <= damageAmount:
			damageAmount -= block
			block = 0
			blockNode.visible = false
			blockText.text = str(block)
		else:
			block -= damageAmount
			damageAmount = 0
			blockText.text = str(block)
	if damageAmount > 0:
		currentHealth -= damageAmount
	if currentHealth < 0:
		die()
	updateHealth()
	updateStatus()

#func _process(delta):
#	pass


