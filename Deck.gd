extends Node2D

var cardToAdd = 0

var deck = [] # this deck holds card ID's, defined in another section
var discard = []
@onready var enemies
@onready var player
# Code for the deck
func _ready():
	#test deck
	#deck.append_array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30])
	deck = get_node("/root/LOADER").deck
	deck.shuffle()
	enemies = get_node("/root/Node2D/GUI/Enemies")
	player = get_node("/root/Node2D/GUI/Player")
	#pass # Replace with function body.


func _process(_delta):
	pass
	

func addToDeck():
	deck.push_front(cardToAdd)

func drawCard():
	var theHand = $"../Hand"
	var myHand = theHand.hand
	
	# if out of cards, shuffle discard into deck
	if(deck.size() == 0):
		shuffleDiscard()
	# add top card to hand, then remove from deck
	myHand.push_front(deck[0])
	deck.remove_at(0)
	
	
func drawHand():
	var theHand = $"../Hand"
	var myHand = theHand.hand
	
	while(myHand.size() < 5):
		drawCard()
	theHand.displayHand()

func shuffleDiscard():
	deck.append_array(discard)
	discard.clear()
	# now shuffle
	deck.shuffle()

func resetDeck():
	var theHand = $"../Hand"
	var myHand = theHand.hand
	
	deck.append_array(myHand)
	deck.append_array(discard)
	discard.clear()
	myHand.clear()
	deck.shuffle()


var cardID = 0
var desc
var cardName
var energy
var picture
var cardPic

func getCardInfo():
	# this follows the card document, in order
	match cardID:
		0:
			desc = "Deals 1 Damage"
			cardName = "Test Card"
			energy = 0
			# picture
			picture = "res://Sprites/EnergyToken.png"
			cardPic = "res://Sprites/warrior card.png"
		1:
			desc = "Deals 3 damage."
			cardName = "Punch"
			energy = 0
			# picture
			picture = "res://Sprites/Punch.png"
			cardPic = "res://Sprites/warrior card.png"
		2:
			desc = "Deals 6 damage."
			cardName = "Swing"
			energy = 1
			# picture
			picture = "res://Sprites/Swing.png"
			cardPic = "res://Sprites/warrior card.png"
		3:
			desc = "Add 5 shield."
			cardName = "Block"
			energy = 1
			# picture
			picture = "res://Sprites/Block.png"
			cardPic = "res://Sprites/warrior card.png"
		4:
			desc = "Deals 3 damage, weakens enemy."
			cardName = "Club"
			energy = 1
			# picture
			picture = "res://Sprites/Club.png"
			cardPic = "res://Sprites/warrior card.png"
		5:
			desc = "Deals 8 Damage to all enemies."
			cardName = "Cleave"
			energy = 2
			# picture
			picture = "res://Sprites/cleave.png"
			cardPic = "res://Sprites/warrior card.png"
		6:
			desc = "Hits 3 random times for 7 damage."
			cardName = "Flurry Strike"
			energy = 3
			# picture
			picture = "res://Sprites/Flurry Strike.png"
			cardPic = "res://Sprites/warrior card.png"
		7:
			desc = "Deals 8 bleeding damage."
			cardName = "Bleeding Strike"
			energy = 2
			# picture
			picture = "res://Sprites/Bleeding Strike.png"
			cardPic = "res://Sprites/warrior card.png"
		8:
			desc = "Deals 10 damage if the player has less than half health, otherwise does 5 damage."
			cardName = "Wounded Rush"
			energy = 1
			# picture
			picture = "res://Sprites/wounded rush.png"
			cardPic = "res://Sprites/warrior card.png"
		9:
			desc = "Deal 20 damage to an enemy, and 5 damage to yourself."
			cardName = "Brazen Charge"
			energy = 2
			# picture
			picture = "res://Sprites/Brazen Charge.png"
			cardPic = "res://Sprites/warrior card.png"
		10:
			desc = "Lose all shields, dealing a damage for each shield lost this way."
			cardName = "Shield Bash"
			energy = 3
			# picture
			picture = "res://Sprites/Shield Bash.png"
			cardPic = "res://Sprites/warrior card.png"
		11:
			desc = "Deals 2 piercing damage."
			cardName = "Throw Dart"
			energy = 0
			# picture
			picture = "res://Sprites/Throw Dart.png"
			cardPic = "res://Sprites/Ranger Card.png"
		12:
			desc = "Deals 4 piercing damage."
			cardName = "Shoot"
			energy = 1
			# picture
			picture = "res://Sprites/Shoot.png"
			cardPic = "res://Sprites/Ranger Card.png"
		13:
			desc = "Avoid the next instance of damage."
			cardName = "Dodge"
			energy = 1
			# picture
			picture = "res://Sprites/Dodge.png"
			cardPic = "res://Sprites/Ranger Card.png"
		14:
			desc = "Enemies take 2 damage each time they attack."
			cardName = "Caltrops"
			energy = 1
			# picture
			picture = "res://Sprites/Caltrops.png"
			cardPic = "res://Sprites/Ranger Card.png"
		15:
			desc = "Hit each enemy for 6 piercing damage."
			cardName = "Multishot"
			energy = 2
			# picture
			picture = "res://Sprites/Multishot.png"
			cardPic = "res://Sprites/Ranger Card.png"
		16:
			desc = "Hit 3 random enemies for 5 piercing damage."
			cardName = "Rapid Fire"
			energy = 3
			# picture
			picture = "res://Sprites/rapid fire.png"
			cardPic = "res://Sprites/Ranger Card.png"
		17:
			desc = "Deals 16 piercing damage."
			cardName = "Sniper Shot"
			energy = 3
			# picture
			picture = "res://Sprites/Sniper Shot.png"
			cardPic = "res://Sprites/Ranger Card.png"
		18:
			desc = "Deals 8 poison damage."
			cardName = "Poison Arrow"
			energy = 2
			# picture
			picture = "res://Sprites/Poison Arrow.png"
			cardPic = "res://Sprites/Ranger Card.png"
		19:
			desc = "Shoots 8 arrows, each with a chance to miss. Every hit deals 4 piercing damage."
			cardName = "Octo-Shot"
			energy = 2
			# picture
			picture = "res://Sprites/octoshot.png"
			cardPic = "res://Sprites/Ranger Card.png"
		20:
			desc = "If you are dodging, deals 24 piercing damage. Otherwise, deals 10 piercing damage."
			cardName = "Sneak Shot"
			energy = 3
			# picture
			picture = "res://Sprites/Sneak Shot.png"
			cardPic = "res://Sprites/Ranger Card.png"
		21:
			desc = "Gain a magic orb."
			cardName = "Create Orb"
			energy = 0
			# picture
			picture = "res://Sprites/create orb.png"
			cardPic = "res://Sprites/Mage Card.png"
		22:
			desc = "Deals 3 damage for 3 turns."
			cardName = "Hurl Flame"
			energy = 1
			# picture
			picture = "res://Sprites/hurl flame.png"
			cardPic = "res://Sprites/Mage Card.png"
		23:
			desc = "Add 5 shield."
			cardName = "Barrier"
			energy = 1
			# picture
			picture = "res://Sprites/Barrier.png"
			cardPic = "res://Sprites/Mage Card.png"
		24:
			desc = "Deals 5 damage for 4 turns."
			cardName = "Fireball"
			energy = 2
			# picture
			picture = "res://Sprites/fireball.png"
			cardPic = "res://Sprites/Mage Card.png"
		25:
			desc = "Deal 5 damage, weakens the enemy, and stuns them for one turn."
			cardName = "Freeze"
			energy = 2
			# picture
			picture = "res://Sprites/freeze.png"
			cardPic = "res://Sprites/Mage Card.png"
		26:
			desc = "Hits all enemies for 6 damage, the selected enemy is also weakened."
			cardName = "Chain Lightning"
			energy = 2
			# picture
			picture = "res://Sprites/Chain Lightning.png"
			cardPic = "res://Sprites/Mage Card.png"
		27:
			desc = "Gain 2 Shield and 1 energy."
			cardName = "Restore Energy"
			energy = 1
			# picture
			picture = "res://Sprites/restore energy.png"
			cardPic = "res://Sprites/Mage Card.png"
		28:
			desc = "Restore 15 player HP."
			cardName = "Restore HP"
			energy = 2
			# picture
			picture = "res://Sprites/Restore Health.png"
			cardPic = "res://Sprites/Mage Card.png"
		29:
			desc = "Deals 3 fire damage for 3 turns, stuns and weakens the enemy, and chains lightning."# 4 damage lightning
			cardName = "Elemental Blast"
			energy = 3
			# picture
			picture = "res://Sprites/elemental blast.png"
			cardPic = "res://Sprites/Mage Card.png"
		30:
			desc = "Deals 1 damage, plus 1 damage for each orb you have."
			cardName = "Orb Smash"
			energy = 1
			# picture
			picture = "res://Sprites/orb smash.png"
			cardPic = "res://Sprites/Mage Card.png"
		_:
			# If nothing matches, run this block of code
			push_error("This card ID has not been created yet! Double check the ID, or create a new card here!")
	
var useID = 0 # this is card position (0 - 4) not cardID
func activateCard():
	print("sucess", useID)
	var theHand = get_node("/root/Node2D/Hand")
	var myHand = theHand.hand
	
	var cardToUse = myHand[useID]
	cardID = cardToUse
	getCardInfo() # now energy has been identified
	
	var compareEnergy = player.energyCurrent
	if(cardToUse > 21): # if mage card and not orb
		compareEnergy = player.magicOrbs
	if(compareEnergy >= energy):
		if(cardToUse > 21): # if mage card and not orb
			player.addOrb(-energy)
		else: #do normal energy
			player.addEnergy(-energy)
		# massive switch statement for cardToUse
		# Note: As code goes, this is a little ugly, but this is done to avoid making scripts for EVERY card type
		match cardToUse:
			0:
				dealDamage(1)
			1:
				dealDamage(3)
			2:
				dealDamage(6)
			3:
				addBlock(5)
			4:
				dealDamage(3)
				addWeakness()
			5:
				dealDamage(8, null, null, true)
			6:
				dealDamage(7, null, null, false, 3)
			7:
				statusEffect("Bleed", 8, 2) # 8 bleed for 2 turns
			8:
				if(player.currentHealth < (player.maxHealth / 2)):
					dealDamage(10)
				else: 
					dealDamage(5)
			9:
				dealDamage(20)
				player.takeDamage(5)
			10:
				dealDamage(player.block)
				addBlock(-player.block)
			11:
				dealDamage(2, true)
			12:
				dealDamage(4, true)
			13:
				player.addDodge(1)
			14:
				for en in enemies.enemylist:
					en.selfDamage += 2
			15:
				dealDamage(6, true, null, true)
			16:
				dealDamage(5, true, null, false, 3)
			17:
				dealDamage(16, true, null)
			18:
				statusEffect("Poison", 8, 2) # 8 poison for 2 turns
			19:
				for n in 8:
					# one third chance of hitting
					if((randi() % 3) == 0):
						dealDamage(4, true)
			20:
				if(player.dodge > 0):
					dealDamage(24, true)
				else:
					dealDamage(10, true)
			21:
				player.addOrb(1)
			22:
				statusEffect("Fire", 3, 3)
			23:
				addBlock(5)
			24:
				statusEffect("Fire", 5, 4)
			25:
				dealDamage(5)
				addWeakness()
				statusEffect("Stun", 1, null)
			26:
				dealDamage(6, null, null, true)
				addWeakness()
			27:
				addBlock(2)
				player.addEnergy(1)
				pass
			28:
				player.heal(15)
			29:
				statusEffect("Fire", 3, 3)
				statusEffect("Stun", 1, null)
				addWeakness()
				dealDamage(4, null, null, true)
			30:
				dealDamage(1+player.magicOrbs)
			_:
				# If nothing matches, run this block of code
				push_error("This card ID has not been created yet! Double check the ID, or create a new card here!")
		
	#	match useID+1:	#for testing. swap with line above if i forget to remove
	#		1:
	#			# If cardToUse == 1, run this block of code
	#			addBlock(5)#add 5 block
	#			pass
	#		2:
	#			dealDamage(10)#hit targeted enemy for 10
	#			pass
	#		3:
	#			dealDamage(5, null, false, 3)# hit 3 random targets for 5
	#			pass
	#		4:
	#			dealDamage(5, null, true)# hit all for 5
	#			pass
	#		_:
	#			# If nothing matches, run this block of code
	#			pass
	#
		# add to discard, remove from hand
		# Note: Should only do this code if you successfully used the card.
		discard.push_front(cardToUse)
		myHand.remove_at(useID)
		theHand.displayHand()
	
func addBlock(amount):
	player.addBlock(amount)
	pass
func dealDamage(amount, piercing=false, type = null, all = false, rand = 0):	#all is bool for hitting all enemies
	enemies.dealDamage(amount, piercing, type, all, rand)					#rand is number of random targets
	pass
func addWeakness():
	enemies.addWeakness()
	pass
func statusEffect(name, damage, turns):
	# Note: name can either be "Fire", "Bleed","Poison", or "Stun". See id 7 for an example
	# Second Note: Images for these have not been implemented yet, so there isn't a way to see if this is succesful (yet)
	enemies.afflictStatus(name, damage, turns)
