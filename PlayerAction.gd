extends Node2D

var index = 0 # change this based on card selected in hand

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func activateCard():
	var theHand = $Hand
	var myHand = theHand.hand
	var cardToUse = myHand[index]
	
	# massive switch statement for cardID
	# Note: As code goes, this is a little ugly, but this is done to avoid making scripts for EVERY card type
	match cardToUse:
		1:
			# If cardToUse == 1, run this block of code
			pass
		2:
			pass
		3:
			pass
		_:
			# If nothing matches, run this block of code
			pass
	
	# add to discard, remove from hand
	# Note: Should only do this code if you successfully used the card.
	var theDeck = $Deck
	var discard = theDeck.discard
	discard.push_front(cardToUse)
	myHand.remove_at(index)
	
	
