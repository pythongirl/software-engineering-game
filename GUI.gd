extends Node

@onready var deck
@onready var hand

# Called when the node enters the scene tree for the first time.
func _ready():
	deck = get_node("/root/Node2D/Deck")
	hand = get_node("/root/Node2D/Hand")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

#parameters to change
var cardOn #index of card to change
var desc
var cardName
var energy
var picture
var cardPic

# functions to call
func setCard():
	var nodeName = "/root/Node2D/GUI/HandDisplay/KinematicBody" + str(cardOn) + "/Card" + str(cardOn) # gets Card1, Card2, ect.
	var myCard = get_node(nodeName)
	
	myCard.visible = true
	myCard.get_node("Name").text = cardName
	myCard.get_node("Desc").text = desc
	myCard.get_node("EnergyCost/EnergyNumber").text = str(energy)
	myCard.get_node("Picture").texture = load(picture)
	myCard.texture = load(cardPic)
	# change picture as well



func _on_death_button_pressed():
	deck.deck.clear()
	deck.discard.clear()
	hand.hand.clear()
	
	get_tree().change_scene_to_file("res://main_menu.tscn")
	
	
	

